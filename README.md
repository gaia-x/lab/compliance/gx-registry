# Gaia-X Lab Registry

[[_TOC_]]

## Gaia-X Trust Framework

For Gaia-X to ensure a higher and unprecedented level of trust in digital platforms, we need to make trust an
easy-to-understand and adopted principle. For this reason, Gaia-X developed
a [Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) – formerly known as Gaia-X
Compliance and Labelling Framework that safeguards data protection, transparency, security, portability, and flexibility
for the ecosystem as well as sovereignty and European Control.

The Trust Framework is the set of rules that define the minimum baseline to be part of the Gaia-X Ecosystem. Those rules
ensure a common governance and the basic levels of interoperability across individual ecosystems while letting the users
in full control of their choices.

In other words, the Gaia-X Ecosystem is the virtual set of participants and service offerings following the requirements
from the Gaia-X Trust Framework.

## Gaia-X Lab Registry Service

The Gaia-X Lab Registry Service is designed to be used by the Gaia-X Lab Compliance Service. However, an API is exposed
for the registry as well, to get the content of the registry as well as verify the validity of signed claims (e.g., Self
Descriptions) by checking the provided certificates against Gaia-X endorsed Trust Anchor certificates.

### Existing deployments

In addition to the [GXDCH](https://gaia-x.eu/gxdch/) instances, the Gaia-X Lab maintains several instances:

| Deployment URL                                                    | Usage                                                 | Content                                                              |
|-------------------------------------------------------------------|-------------------------------------------------------|----------------------------------------------------------------------|
| [`v1`, `v1.x.x`](https://registry.lab.gaia-x.eu/v1/docs/)         | Production-ready trusted anchors and cert validation. | Latest Tagus release. Version deployed on the Clearing Houses        |
| [`v2`, `v2.x.x`](https://registry.lab.gaia-x.eu/v2/docs/)         | Production-ready trusted anchors and cert validation. | Latest stable Loire release. Version deployed on the Clearing Houses |
| [`main`](https://registry.lab.gaia-x.eu/main/docs/)               | Used for playground activities.                       | Latest stable (main branch)                                          |
| [`development`](https://registry.lab.gaia-x.eu/development/docs/) | Used for playground activities.                       | Latest unstable (development branch)                                 |

### Images tags

This repo provides
several [images tags](https://gitlab.com/gaia-x/lab/compliance/gx-registry/container_registry/2802619).

| tag           | content              | example |
|---------------|----------------------|---------|
| `vX`          | latest major version | v1      |
| `vX.Y`        | latest minor version | v1.1    |
| `vX.Y.Z`      | specific version     | v1.1.1  |
| `main`        | latest stable        |         |
| `development` | latest unstable      |         |

Feature branches are also build and push to the container registry.

https://registry.lab.gaia-x.eu/v2204/docs/ is no longer instantiated. It is the implementation of the outdated Trust
Framework 22.04 document.

All key pairs used to sign claims must have at least one of the Trust Anchors in their certificate chain to comply with
the Gaia-X Trust Framework. The Trust anchors are stored on IPFS as an ETSI 119 612 list and advertised on
the [gxdch.eu](gxcdh.eu) dns.
Trust anchors are published using the version of compliance services and the version of ontology.
This publishing method is
described [here](https://gitlab.com/gaia-x/technical-committee/architecture-working-group/architecture-document/-/blob/main/docs/gx_services.md?ref_type=heads#dns-txt-records-and-naming-convention).

Find a list of endorsed trust anchors
here: https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/trust_anchors/

### Branch-off

Regarding end of Tagus implementation and branch-off for Loire release, we changed a bit the branch organization as
follows:

- v1 branch host code for Tagus release and is maintenance mode (fixes only)
- development host v2/Loire latest code
- main host v2/Loire latest stable code and triggers v2 image releases

### Gaia-X Lab Compliance Service

The Compliance Service will validate the shape and content of Self Descriptions. Required fields and consistency rules
are defined in the [Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/trust_anchors/).

The Compliance Service can validate shapes of self-descriptions and sign valid self-descriptions.

GitLab repository: https://gitlab.com/gaia-x/lab/compliance/gx-compliance

## [Get Started with Using The API](./README-api.md)

## [Get Started With Development](./README-developers.md)

## Deployment

A helm chart is provided inside `/k8s/gx-registry` folder. It deploys the registry application and a kubo IPFS node used
by the registry.

It provides several environment variables for the application:

| Env Variable          | Name in values file            | Default value                                                      | Note                                                                                                                                                                                |
|-----------------------|--------------------------------|--------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| PRIVATE_KEY*          | PRIVATE_KEY                    | an example string                                                  | Put the PEM-encoded private key string directly                                                                                                                                     |
| X509_CERTIFICATE*     | X509_CERTIFICATE               | an example string                                                  | Put the PEM-encoded X509 string directly                                                                                                                                            |
| PRIVATE_KEY_ALGORITHM | PRIVATE_KEY_ALGORITHM          | PS256                                                              | the private key signature algorithm such as the ones described in the [`JsonWebSignature2020` library readme](https://gitlab.com/gaia-x/lab/json-web-signature-2020#key-algorithms) |
| evsslonly             | evsslonly                      | true                                                               | Whether the app is deployed on a production environment. Will enable EV SSL-only validation                                                                                         |
| trustedIssuersURL     | trustedIssuersURL              | https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/trusted-gxdch.yaml  | List of endpoint for each component of accredited GXDCH                                                                                                                             |
| revocationListURL     | revocationListURL              | https://gitlab.com/gaia-x/lab/gxdch/-/raw/main/revoked-issuers.txt | List of certificate no longer trusted after emitter was revoked from using Gaia-X                                                                                                   |
| ONTOLOGY_VERSION      | {{ .Values.ontologyVersion }}  | development                                                        | Version of the Gaia-X Ontology to use                                                                                                                                               |
| APP_BRANCH            | {{ .Values.nameOverride }}     | main                                                               | Deployment branch of the application                                                                                                                                                |
| APP_PATH              | ingress.hosts[0].paths[0].path | /main                                                              | Deployment path of the application                                                                                                                                                  |
| BASE_URL              |                                | https://<ingress.hosts[0].host>/<ingress.hosts[0].paths[0].path>   | URL of the deployed application                                                                                                                                                     |
| BASE_URI              |                                | https://<ingress.hosts[0].host>/<ingress.hosts[0].paths[0].path>   | URL of the deployed application. Iso to BASE_URL                                                                                                                                    |

> \* You can encode directly your pem files in base64 using the following command:

```shell
cat mycertificatefile.crt | base64 -w 0
cat mykeyfile.pem | base64 -w 0
``` 

In case you need it, a bash script is available in `k8s/prepare-yaml.sh`. This scripts outputs a `custom-values.yaml`
file, containing your private key & certificate, as well as your ingress configuration.  
It's used for convenience, because handling a x509certificate on the command line is cumbersome.

You'll need to have two environment variables, `registryKey` & `registryCert` as well as provide two parameter, the app
version (eg v1, development, v2) & the domain the app will be deployed.

Usage:

```shell
./k8s/prepare-yaml.sh registry.lab.gaia-x.eu development \
&& helm upgrade -n development --create-namespace gx-registry -f k8s/custom-values.yaml
```

Usage example without the shell script call:

```shell
helm upgrade --install -n "<branch-name>" --create-namespace gx-registry ./k8s/gx-registry --set "nameOverride=<branch-name>,ingress.hosts[0].host=registry.lab.gaia-x.eu,ingress.hosts[0].paths[0].path=/<branch-name>,image.tag=<branch-name>,ingress.hosts[0].paths[0].pathType=Prefix,evsslonly=false,PRIVATE_KEY=$registryKey,X509_CERTIFICATE=$x509Certificate,PRIVATE_KEY_ALGORITHM=$registryKeyAlg"
```

Deploy a tag:

```shell
helm upgrade --install -n "v2" --create-namespace gx-registry ./k8s/gx-registry --set "nameOverride=v2,ingress.hosts[0].host=registry.lab.gaia-x.eu,ingress.hosts[0].paths[0].path=/v2,image.tag=v2,ingress.hosts[0].paths[0].pathType=Prefix,PRIVATE_KEY=$registryKey,X509_CERTIFICATE=$x509Certificate,PRIVATE_KEY_ALGORITHM=$registryKeyAlg"
```

The deployment is triggered automatically on `development` and `main` branches. Please refer
to [Gaia-X Lab Registry Service](#gaia-x-lab-registry-service) for available instances.

### V2 upgrade

Due to major changes in the chart for V2, we recommend you uninstall your already installed gx-registry and install this
new one.

```shell
helm uninstall -n <namespace> gx-registry
```
