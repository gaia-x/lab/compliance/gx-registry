#!/bin/bash

if [ $# -ne 3 ]; then
    echo "Usage: $0 <deploymentUrl> <version> <ontologyVersion>"
    exit 1
fi

deploymentUrl=$1
version=$2
ontologyVersion=$3

# Check if environment variables exist
if [ -z "$registryKey" ] || [ -z "$registryCert" ]; then
    echo "Error: Environment variables registryKey or registryCert are not set."
    exit 1
fi

# Create a YAML file and handle multiline values
cat <<EOF >custom-values.yaml
nameOverride: $version
image:
  tag: $version
ingress:
  hosts:
   - host: $deploymentUrl
     paths:
       - path: /$version
         pathType: Prefix
PRIVATE_KEY: $(echo "$registryKey" | base64 -w 0)
X509_CERTIFICATE: $(echo "$registryCert" | base64 -w 0)
ontologyVersion: $ontologyVersion
EOF


if [ ! -z "$ISEVSSLONLY" ]; then
    echo "evsslonly: $ISEVSSLONLY" >> custom-values.yaml
else
    echo "evsslonly: true" >> custom-values.yaml
fi