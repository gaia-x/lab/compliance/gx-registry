# Get Started with Using The API
[[_TOC_]]

You can find the Swagger API documentation under `localhost:3000/docs/` or https://registry.lab.gaia-x.eu/docs/v1

## Checks on certificates

The following checks are done sequentially on certificate chain for their validation:

- The certificate chain must be continuous from leaf to root
- Certificates signatures in the chain must be valid
- Certificates expiration date must be in the future
- The root certificate must come from an AC, or be registered as a trusted anchor in the registry

## Look for a given Trust Anchor public key in the registry

Using the `/api/trustAnchor` route, you can check if a given public key exists in the list of endorsed trust anchors.
You must provide the public key in the request body.

**Request body:**

```json
{
  "certificate": "MIIE0TCCA7mgAwIBAgICApMwDQYJKoZIhvcNAQEFBQAwgc8xCzAJBgNVBAYTAkFUMYGLMIGIBgNVBAoegYAAQQAtAFQAcgB1AHMAdAAgAEcAZQBzAC4AIABmAPwAcgAgAFMAaQBjAGgAZQByAGgAZQBpAHQAcwBzAHkAcwB0AGUAbQBlACAAaQBtACAAZQBsAGUAawB0AHIALgAgAEQAYQB0AGUAbgB2AGUAcgBrAGUAaAByACAARwBtAGIASDEYMBYGA1UECxMPQS1UcnVzdC1RdWFsLTAxMRgwFgYDVQQDEw9BLVRydXN0LVF1YWwtMDEwHhcNMDIwMjA2MjMwMDAwWhcNMDUwMjA2MjMwMDAwWjCB0TELMAkGA1UEBhMCQVQxgYswgYgGA1UECh6BgABBAC0AVAByAHUAcwB0ACAARwBlAHMALgAgAGYA/AByACAAUwBpAGMAaABlAHIAaABlAGkAdABzAHMAeQBzAHQAZQBtAGUAIABpAG0AIABlAGwAZQBrAHQAcgAuACAARABhAHQAZQBuAHYAZQByAGsAZQBoAHIAIABHAG0AYgBIMRkwFwYDVQQLExBUcnVzdFNpZ24tU2lnLTAxMRkwFwYDVQQDExBUcnVzdFNpZ24tU2lnLTAxMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3vqE78j1NAlAT8GC1kqNk6qL+jn1dLTADCiQVIyct01vkO0zINkUg2x6k7E2xnmyDPg4ihayczDYcuzzQMMms63MFDlONMZ180CxFsZpOYK8SctdTWdH0tGJFJF0oBWI6ROykMxo3knPLMDMTS586MgPdKxmihk5OjyZvqiEH1SdmuFR4UTke81cisUkM6+hh4v/AYrPShXOlEmNmFILMw9iQST5aeylmgfWwixCZgnmS7fr3j65gezxhPN56qRyndgu97/9VsKMASYTsaD8pZEZ230TIopuwGb4AeO7CUrU22scpnreHLW1ZX4hldJYJXzFpBH2pAnHfiQgADs4CwIDAQABo4GyMIGvMA8GA1UdEwEB/wQFMAMBAf8wEQYDVR0OBAoECEeMFJHO+gwZMBMGA1UdIwQMMAqACEs8jB2F6W+tMA4GA1UdDwEB/wQEAwIBBjBkBgNVHR8EXTBbMFmgV6BVhlNsZGFwOi8vbGRhcC5hLXRydXN0LmF0L291PUEtVHJ1c3QtUXVhbC0wMSxvPUEtVHJ1c3QsYz1BVD9jZXJ0aWZpY2F0ZXJldm9jYXRpb25saXN0PzANBgkqhkiG9w0BAQUFAAOCAQEAS1Ch3C9IGqX4c/PCp0Hp93r+07ftu+A77EW7w5kDQ6QhuuXAJgVk4Vyp4D+wlJMxQpvDGkBnaSYcRyT5rT7ie/eImbHi+ep6AD/mcKs8D2XXDDEy/UxuvEe7pDqrAZc93LID8HnrMf2OFqXbmBggN/TvD20s2CBnEgP+QCm9asttMCg1FLIAIeDL/JstH3ddTlIQNPgUCCaUmATP8oOnh2eon7Fn5+dnKDUY4j6lRDPsDzu0wU0s9VWiyS9Lay0f7P9h3LcYHtAYZg2BPlEAqpOVNVXZ+4JdjRKvhHh9pgadnGNkunrenDiekHgbABop0s2yj5EDAkqYIosnp4bOCQ=="
}
```

**Responses:**

If a corresponding trust anchor is found, a HTTP response status code `200` and a response object will be returned.

**Response object:**

HTTP response status code `200`:

```json
{
  "trustState": "trusted",
  "trustedForAttributes": "/.*/gm",
  "trustedAt": 1645817070
}
```

**Negative responses:**
|HTTP response status code |Description |
|--- | --- |
|`404`| Could not find a Trust Anchor for the given public key|
|`400`| Invalid request|

## Verify that a given Certificate Chain is resolvable against a Trust Anchor in the registry

Using the `/api/trustAnchor/chain` route, you can check if any of the given certificates in a certificate chain is
resolvable against an endorsed trust anchor.
You must provide the certificate chain as `String` in the request body. Make sure to provide a cleared `String` without
spaces and new line characters.

**Request body (certificates shortened in example):**

```json
{
  "certs": "-----BEGIN CERTIFICATE-----MIIDxzCCA02gAwIBAgISAwrdU+...sF6DpICK1MndBOGAY5E5RDu1EW6+Snk852PQTDM=-----END CERTIFICATE----------BEGIN CERTIFICATE-----MIICxjCCAk2gAwIBAgIRALO93.../Lgul/-----END CERTIFICATE----------BEGIN CERTIFICATE-----MIICGzCCAaGgAwIBAgIQQdKd0XLq7qeAwSxs6S...q4AaOeMSQ+2b1tbFfLn-----END CERTIFICATE-----"
}
```

**Responses:**

If a corresponding trust anchor is found, a HTTP response status code `200` and a response object will be returned.

**Response object:**

HTTP response status code `200`:

```json
{
  "result": true
}
```

**Negative responses:**

HTTP response status code `409` - Certificate chain could not be verified:

```json
{
  "result": false,
  "resultCode": -1,
  "resultMessage": "No valid certificate paths found"
}
```

| HTTP response status code | Description     |
|---------------------------|-----------------|
| `400`                     | Invalid request |
