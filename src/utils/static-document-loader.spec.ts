import staticDocumentLoader from './static-document-loader'

describe('static-document-loader', () => {
  it('should return local context for jws', async () => {
    const vcCtxt = await staticDocumentLoader('https://www.w3.org/2018/credentials/v1', null)
    expect(vcCtxt).toBeDefined()
    expect(vcCtxt.document).toBeDefined()
    expect(vcCtxt.document).toContain('VerifiableCredential')
  })
  it('should return try to fetch from original loader with an unknown context', async () => {
    const vcCtxt = await staticDocumentLoader('https://w3id.org/security#', null)
    expect(vcCtxt).toBeDefined()
  }, 10_000)
})
