import { Module } from '@nestjs/common'
import { ContextController } from './context.controller'
import { ContextService } from './service'

@Module({
  imports: [],
  controllers: [ContextController],
  providers: [ContextService]
})
export class ContextModule {}
