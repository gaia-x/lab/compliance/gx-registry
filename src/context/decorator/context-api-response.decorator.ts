import { applyDecorators } from '@nestjs/common'
import { ApiBadRequestResponse, ApiOkResponse, ApiOperation, ApiParam } from '@nestjs/swagger'

export function ContextFilesApiResponse(summary: string) {
  return applyDecorators(
    ApiOperation({ summary }),
    ApiParam({
      name: 'version',
      allowEmptyValue: true,
      required: false,
      description: 'Version of the desired context (defaults to `development`)',
      examples: {
        empty: {
          value: '',
          description: 'Defaults to the latest development version'
        },
        development: {
          value: 'development',
          description: 'For the latest development version'
        },
        '2406': {
          value: '2406',
          description: 'For version 24.06 of the ontology'
        }
      }
    }),
    ApiBadRequestResponse({ description: 'Context not found' }),
    ApiOkResponse({ description: 'Context as JSON-LD' }) // TODO add me
  )
}
