import { Controller, Get, Param, Response } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { Response as Res } from 'express'
import { ContextFilesApiResponse } from './decorator'
import { ContextService } from './service'

@ApiTags('context-registry')
@Controller({ path: '/' })
export class ContextController {
  constructor(private readonly contextService: ContextService) {}

  @ContextFilesApiResponse('Get the specified JSON-LD context version')
  @Get(['/context/:version?'])
  getContextVersion(@Response({ passthrough: true }) res: Res, @Param('version') version?: string): string {
    try {
      res.set({ 'Content-Type': 'application/ld+json' })

      return this.contextService.getContextFile(version)
    } catch (e) {
      console.log('Unable to get context', e)
      throw e
    }
  }
}
