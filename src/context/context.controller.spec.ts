import { INestApplication } from '@nestjs/common'
import { Test, TestingModule } from '@nestjs/testing'
import request from 'supertest'
import { ContextModule } from './context.module'
import { ContextService } from './service'

describe('ContextController', () => {
  let app: INestApplication
  let contextService: ContextService

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [ContextModule]
    }).compile()

    contextService = moduleRef.get<ContextService>(ContextService)
    ;(contextService as any).contextBasePath = `${__dirname}/../tests/fixtures/context`

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  it(
    'should return the latest development context',
    () =>
      request(app.getHttpServer())
        .get('/context')
        .expect(200)
        .expect('Content-Type', 'application/ld+json; charset=utf-8')
        .then(response => {
          expect(response.text.indexOf('"gx": "https://w3id.org/gaia-x/development#"')).toBeGreaterThan(-1)
        }),
    10000
  )

  it(
    'should return the 1234 version context',
    () =>
      request(app.getHttpServer())
        .get('/context/1234')
        .expect(200)
        .expect('Content-Type', 'application/ld+json; charset=utf-8')
        .then(response => {
          expect(response.text.indexOf('"gx": "https://w3id.org/gaia-x/1234#"')).toBeGreaterThan(-1)
        }),
    10000
  )

  it(
    'should throw a 404 error when querying a non-existent context version',
    () => request(app.getHttpServer()).get('/context/non-existent').expect(404).expect('Content-Type', 'application/json; charset=utf-8'),
    10000
  )
})
