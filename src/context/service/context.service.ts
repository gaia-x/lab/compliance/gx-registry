import { Injectable, NotFoundException } from '@nestjs/common'
import fs from 'fs'

@Injectable()
export class ContextService {
  private contextBasePath = '/data/ipfs/registry'

  getContextFile(version: string = 'development'): string {
    try {
      const filePath: string = `${this.contextBasePath}/${version}/schemas/trustframework.json`
      fs.accessSync(filePath)

      return fs.readFileSync(filePath).toString()
    } catch {
      throw new NotFoundException(`Desired context version '${version}' not found or not accessible`)
    }
  }
}
