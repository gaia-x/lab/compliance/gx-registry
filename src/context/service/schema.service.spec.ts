import fs from 'fs'
import { ContextService } from './context.service'

jest.mock('fs')

describe('ContextService', () => {
  const service: ContextService = new ContextService()

  beforeEach(() => {
    jest.resetAllMocks()
  })

  it('should provide the development context file by default', () => {
    jest.mocked(fs.accessSync).mockReturnValue(undefined)
    jest.mocked(fs.readFileSync).mockReturnValue(Buffer.from('Test context\nWith multiple lines'))

    expect(service.getContextFile()).toEqual('Test context\nWith multiple lines')

    expect(fs.accessSync).toBeCalledWith('/data/ipfs/registry/development/schemas/trustframework.json')
    expect(fs.readFileSync).toBeCalledWith('/data/ipfs/registry/development/schemas/trustframework.json')
  })

  it('should provide a specific version of the context file', () => {
    jest.mocked(fs.accessSync).mockReturnValue(undefined)
    jest.mocked(fs.readFileSync).mockReturnValue(Buffer.from('Test context\nWith multiple lines'))

    expect(service.getContextFile('2404')).toEqual('Test context\nWith multiple lines')

    expect(fs.accessSync).toBeCalledWith('/data/ipfs/registry/2404/schemas/trustframework.json')
    expect(fs.readFileSync).toBeCalledWith('/data/ipfs/registry/2404/schemas/trustframework.json')
  })

  it('should throw an exception when file is not accessible', () => {
    jest.mocked(fs.accessSync).mockImplementation(() => {
      throw new Error('Test Error')
    })

    expect(() => service.getContextFile()).toThrow("Desired context version 'development' not found or not accessible")

    expect(fs.accessSync).toBeCalledWith('/data/ipfs/registry/development/schemas/trustframework.json')
    expect(fs.readFileSync).not.toHaveBeenCalled()
  })

  it('should throw an exception when file is not readable', () => {
    jest.mocked(fs.accessSync).mockReturnValue(undefined)
    jest.mocked(fs.readFileSync).mockImplementation(() => {
      throw new Error('Test Error')
    })

    expect(() => service.getContextFile('1234')).toThrow("Desired context version '1234' not found or not accessible")

    expect(fs.accessSync).toBeCalledWith('/data/ipfs/registry/1234/schemas/trustframework.json')
    expect(fs.readFileSync).toBeCalledWith('/data/ipfs/registry/1234/schemas/trustframework.json')
  })
})
