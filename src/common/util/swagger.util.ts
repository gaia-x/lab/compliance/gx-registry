import { INestApplication } from '@nestjs/common'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { writeFileSync } from 'fs'
import * as path from 'path'
import { description, name, version } from '../../../package.json'
import { ConfigurationModule } from '../../configuration/configuration.module'
import { ContextModule } from '../../context/context.module'
import { LinkMLOntologyModule } from '../../linkml-ontology/linkml-ontology.module'
import { OwlOntologyModule } from '../../owl-ontology/owl-ontology.module'
import { TrustedShapeRegistryModule } from '../../shape/trusted-shape-registry.module'
import { TrustAnchorModule } from '../../trust-anchor/trust-anchor.module'
import { TrustedIssuersModule } from '../../trusted-issuers/trusted-issuers.module'

export const OPEN_API_DOC_PATH = path.resolve(process.cwd(), 'openapi.json')

export const SWAGGER_UI_PATH = '/docs'

const options = {
  tagsSorter: 'alpha',
  operationsSorter: 'alpha',
  customCss: `.curl-command { display: none } .swagger-ui .topbar { display: none }; `
}

const versions = [
  {
    number: version,
    latest: true,
    includedModules: [
      ConfigurationModule,
      TrustAnchorModule,
      TrustedIssuersModule,
      ContextModule,
      TrustedShapeRegistryModule,
      OwlOntologyModule,
      LinkMLOntologyModule
    ]
  }
]

export function setupSwagger(app: INestApplication) {
  for (const version of versions) {
    const config = new DocumentBuilder().setTitle(name).setDescription(description).setVersion(version.number).build()
    const document = SwaggerModule.createDocument(app, config, {
      ignoreGlobalPrefix: false,
      include: version.includedModules
    })
    const versionPath = `v${version.number.split('.')[0]}`
    const appPath = process.env['APP_PATH'] ? process.env['APP_PATH'] : ''
    writeFileSync(version.latest ? OPEN_API_DOC_PATH : OPEN_API_DOC_PATH.replace('.json', `-${versionPath}.json`), JSON.stringify(document), {
      encoding: 'utf8'
    })

    SwaggerModule.setup(`${appPath}${SWAGGER_UI_PATH}/${versionPath}`, app, document, options)
    if (version.latest) SwaggerModule.setup(`${appPath}${SWAGGER_UI_PATH}`, app, document, options)
  }
}
