import { splitPEM, stripPEMInfo } from './stripPem'

const singleCert = '-----BEGIN CERTIFICATE-----abc/de+f-----END CERTIFICATE-----'

const singleCertWithLf = `
-----BEGIN PKCS7-----
abc/de+f
-----END CERTIFICATE-----
`

const pem =
  '-----BEGIN CERTIFICATE-----abc/de+fgh-----END CERTIFICATE----------BEGIN CERTIFICATE-----abc/def/gh==-----END CERTIFICATE----------BEGIN CERTIFICATE-----abc/def/gh+-----END CERTIFICATE-----'

const pemWithLf = `
-----BEGIN PKCS7-----
abc/de+fgh
-----END PKCS7-----
-----BEGIN CERTIFICATE-----
abc/def/gh==
-----END CERTIFICATE-----

-----BEGIN CERTIFICATE-----
abc/def/gh+
-----END CERTIFICATE-----
`

describe('stripPem util', () => {
  describe('Strip PEM info', () => {
    it('Should strip PEM info when it is compacted', () => {
      expect(stripPEMInfo(singleCert)).toEqual('abc/de+f')
    })
    it('Should strip PEM info when it has line feeds', () => {
      expect(stripPEMInfo(singleCertWithLf)).toEqual('abc/de+f')
    })
  })
  describe('Split pem certificate chain', () => {
    it('should split compacted certificate chain', () => {
      expect(splitPEM(pem)).toEqual(['abc/de+fgh', 'abc/def/gh==', 'abc/def/gh+'])
    })
    it('should split certificate chain with line feeds', () => {
      expect(splitPEM(pemWithLf)).toEqual(['abc/de+fgh', 'abc/def/gh==', 'abc/def/gh+'])
    })
  })
})
