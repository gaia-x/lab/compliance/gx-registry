import { isNotEmpty } from '../util/index'

export function stripPEMInfo(PEMInfo: string): string {
  const stripped = PEMInfo.replace(/\s+/g, '')
  return stripped.replace(/-{5}(BEGIN|END)(CERTIFICATE|PKCS7|PRIVATEKEY|PUBLICKEY)-{5}/g, '')
}

export function splitPEM(pem: string): string[] {
  return pem
    .split(/-----BEGIN (?:CERTIFICATE|PKCS7)-----/)
    .map(stripPEMInfo)
    .filter(isNotEmpty)
}
