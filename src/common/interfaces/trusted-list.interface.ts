export interface TrustedList {
  TrustServiceStatusList: {
    TrustServiceProviderList: {
      TrustServiceProvider: Partial<TrustServiceProvider>[]
    }
    SchemeInformation: { ListIssueDateTime: string }
  }
}

export interface TrustServiceProvider {
  TSPServices: {
    TSPService: Partial<TSPService>
  }
}

export interface TSPService {
  ServiceInformation: {
    ServiceDigitalIdentity: {
      DigitalId: {
        X509Certificate: string
      }
    }
  }
}
