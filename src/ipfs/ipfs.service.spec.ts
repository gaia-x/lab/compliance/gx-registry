import { jest } from '@jest/globals'
import { Test, TestingModule } from '@nestjs/testing'
import { promises as fs } from 'fs'
import { CID } from 'kubo-rpc-client/dist/src/types'
import { IpfsService } from './ipfs.service'

const DOMAIN = 'gxdch.eu'
let originalEnv

const ipfsMock = {
  create: jest.fn().mockImplementation(() => ({
    id: jest.fn().mockReturnValue({ id: 'test-peer-id' }),
    files: {
      stat: jest.fn().mockReturnValue({
        type: 'directory'
      })
    },
    ls: jest.fn().mockImplementation(async function* (passedCid) {
      if (passedCid === 'CIDTEST') {
        yield { name: 'file1.txt', type: 'file', cid: 'CID1' as unknown as CID }
      }
      if (passedCid === 'CIDDIR') {
        yield { name: 'dir', type: 'dir', cid: 'CIDDIRECTORY' as unknown as CID }
      }
    }),
    cat: jest.fn().mockImplementation(async function* (passedCid) {
      if (passedCid === 'CIDTEST') {
        yield new Uint8Array(2)
      }
    }),
    pin: {
      add: jest.fn()
    },
    dht: {
      provide: jest.fn()
    },
    stop: jest.fn()
  })),
  CID: { parse: jest.fn().mockReturnValue('CIDTEST') as unknown as CID }
}

jest.unstable_mockModule('kubo-rpc-client', () => ipfsMock)

jest.mock('fs', () => {
  const originalModule: typeof fs = jest.requireActual('fs')
  return {
    ...originalModule,
    promises: {
      mkdir: jest.fn(),
      writeFile: jest.fn(),
      rename: jest.fn()
    }
  }
})

describe('IpfsService', () => {
  let service: IpfsService

  beforeEach(async () => {
    jest.clearAllMocks()

    originalEnv = process.env
    process.env = { ...originalEnv, APP_BRANCH: 'branchName' }

    const module: TestingModule = await Test.createTestingModule({
      providers: [IpfsService]
    }).compile()

    service = module.get<IpfsService>(IpfsService)
    ;(service as any).getTxtRecords = jest.fn().mockReturnValue([['ipfs://CIDTEST']])
    ;(service as any).ipfs = ipfsMock
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  it('should correctly start IPFS node', async () => {
    await (service as any).startNode()
    expect((service as any).ipfs).toBeDefined()
    expect((service as any).ipfs.id).toBeDefined()
  })

  it('should process TXT records and fetch registry resources', async () => {
    process.env['APP_BRANCH'] = 'branchName'
    ;(service as any).listContentsAndSave = jest.fn()
    await (service as any).startNode()

    expect((service as any).getTxtRecords).toHaveBeenCalledWith(`branchName.${DOMAIN}`)
    expect((service as any).listContentsAndSave).toHaveBeenCalledWith('CIDTEST')
  })

  it('should throw an error if TXT record is not found', async () => {
    ;(service as any).getTxtRecords = jest.fn().mockReturnValue([[]])

    await expect((service as any).fetchRegistryResources()).rejects.toThrow()
  })

  it('should list contents and save files', async () => {
    ;(service as any).saveFileFromIpfsToLocal = jest.fn()
    await (service as any).startNode()
    // Assume CIDTEST points to a directory with one file
    await (service as any).listContentsAndSave('CIDTEST', '')

    // Check if saveFileFromIpfsToLocal was called for the file
    expect(service.saveFileFromIpfsToLocal).toHaveBeenCalledWith('CID1', 'file1.txt')
  })

  it('should recursively handle directories', async () => {
    const listContentsAndSaveSpy = jest.spyOn(service as any, 'listContentsAndSave')
    await (service as any).startNode()

    await (service as any).listContentsAndSave('CIDDIR', '')

    expect(listContentsAndSaveSpy).toHaveBeenCalledWith('CIDDIRECTORY', 'dir')
  })
  it('should save file from IPFS to local', async () => {
    await (service as any).startNode()

    await service.saveFileFromIpfsToLocal('CIDTEST' as unknown as CID, 'path/to/file.txt')

    expect(fs.writeFile).toHaveBeenCalledWith(expect.any(String), expect.any(Buffer))
    expect(fs.mkdir).toHaveBeenCalledWith(expect.any(String), { recursive: true })
    expect(fs.rename).toHaveBeenCalledWith(expect.any(String), expect.any(String))
  })
})
