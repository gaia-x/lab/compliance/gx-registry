import { HttpService } from '@nestjs/axios'
import { Injectable, Logger } from '@nestjs/common'
import { promises as fs } from 'fs'
import path from 'path'
import { firstValueFrom } from 'rxjs'
import YAML from 'yaml'

export type TrustedIssuers = { [key: string]: string[] }

@Injectable()
export class TrustedIssuersService {
  private readonly logger = new Logger(TrustedIssuersService.name)
  private readonly trustedIssuersURL = process.env.trustedIssuersURL
  private isIssuerURLOverriden = false
  private ontologyVersion: string

  constructor(private httpService: HttpService) {
    this.ontologyVersion = process.env.ONTOLOGY_VERSION || 'development'

    if (process.env.trustedIssuersURL) {
      try {
        new URL(this.trustedIssuersURL)
        this.isIssuerURLOverriden = true
      } catch (error) {
        this.logger.error('Invalid format for overriden trusted issuers URL, reverting back to default:', error)
      }
    }
  }

  async getTrustedIssuers(): Promise<TrustedIssuers> {
    if (this.isIssuerURLOverriden) {
      const request = this.httpService.get(this.trustedIssuersURL)
      return YAML.parse((await firstValueFrom(request)).data)
    }

    const filePath = path.join('/data/ipfs/registry', this.ontologyVersion, 'trusted-gxdch.yaml')
    const fileContents = await fs.readFile(filePath, 'utf8')

    return YAML.parse(fileContents)
  }

  async getTrustedIssuersForAServiceType(service: string): Promise<string[]> {
    return (await this.getTrustedIssuers())[service] ? (await this.getTrustedIssuers())[service] : []
  }
}
