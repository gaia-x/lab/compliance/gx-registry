import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { ShapeService } from './services'
import { TrustedShapeRegistry } from './trusted-shape-registry.controller'

@Module({
  imports: [ConfigModule],
  controllers: [TrustedShapeRegistry],
  providers: [ShapeService]
})
export class TrustedShapeRegistryModule {}
