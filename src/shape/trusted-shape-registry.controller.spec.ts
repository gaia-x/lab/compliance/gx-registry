import { INestApplication } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Test, TestingModule } from '@nestjs/testing'
import request from 'supertest'
import { ConfigServiceMock } from '../tests/utils/config-service.mock'
import { ShapeService } from './services'
import { TrustedShapeRegistryModule } from './trusted-shape-registry.module'

describe('TrustedShapeRegistryController', () => {
  let app: INestApplication
  let shapeService: ShapeService

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [TrustedShapeRegistryModule]
    })
      .overrideProvider(ConfigService)
      .useValue(new ConfigServiceMock().setProperty('BASE_URL', 'https://gaia-x.eu'))
      .compile()

    shapeService = moduleRef.get<ShapeService>(ShapeService)
    ;(shapeService as any).shapesBasePath = `${__dirname}/../tests/fixtures/shapes`

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  it(
    'should return the latest development shape',
    () =>
      request(app.getHttpServer())
        .get('/shapes')
        .expect(200)
        .expect('Content-Type', 'text/turtle; charset=utf-8')
        .then(response => {
          expect(response.text.indexOf('gx:LegalParticipant')).toBeGreaterThan(-1)
        }),
    10000
  )

  it(
    'should return the 1234 version shape',
    () =>
      request(app.getHttpServer())
        .get('/shapes/1234')
        .expect(200)
        .expect('Content-Type', 'text/turtle; charset=utf-8')
        .then(response => {
          expect(response.text.indexOf('gx:LegalPerson')).toBeGreaterThan(-1)
        }),
    10000
  )

  it(
    'should throw a 404 error when querying a non-existent version',
    () => request(app.getHttpServer()).get('/shapes/non-existent').expect(404).expect('Content-Type', 'application/json; charset=utf-8'),
    10000
  )
})
