import { Controller, Get, Param, Response } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { Response as Res } from 'express'
import { ShapeFilesApiResponse } from './decorators'
import { ShapeService } from './services'

@ApiTags('Trusted-Shape-registry')
@Controller({ path: '/shapes' })
export class TrustedShapeRegistry {
  constructor(private readonly shapesService: ShapeService) {}

  @ShapeFilesApiResponse('Get a version of the Gaia-X ontology SHACL shapes in Turtle format')
  @Get(':version?')
  getShape(@Response({ passthrough: true }) res: Res, @Param('version') version?: string): string {
    try {
      res.set({ 'Content-Type': 'text/turtle' })

      return this.shapesService.getTTLFile(version)
    } catch (e) {
      throw e
    }
  }
}
