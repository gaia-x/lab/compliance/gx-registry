import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { ScheduleModule } from '@nestjs/schedule'
import { ServeStaticModule } from '@nestjs/serve-static'
import { AppController } from './app.controller'
import { ConfigurationModule } from './configuration/configuration.module'
import { ContextModule } from './context/context.module'
import { IpfsModule } from './ipfs/ipfs.module'
import { LinkMLOntologyModule } from './linkml-ontology/linkml-ontology.module'
import { OwlOntologyModule } from './owl-ontology/owl-ontology.module'
import { TrustedShapeRegistryModule } from './shape/trusted-shape-registry.module'
import { TrustAnchorModule } from './trust-anchor/trust-anchor.module'
import { TrustedIssuersModule } from './trusted-issuers/trusted-issuers.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      cache: true,
      isGlobal: true
    }),
    ScheduleModule.forRoot(),
    ServeStaticModule.forRoot({
      rootPath: process.env.IPFS_REGISTRY_PATH || '/data/ipfs/registry',
      serveRoot: process.env.APP_PATH,
      exclude: ['/api*', '/.well-known*']
    }),
    ConfigurationModule,
    IpfsModule,
    TrustAnchorModule,
    TrustedIssuersModule,
    TrustedShapeRegistryModule,
    ContextModule,
    OwlOntologyModule,
    LinkMLOntologyModule
  ],
  controllers: [AppController]
})
export class AppModule {}
