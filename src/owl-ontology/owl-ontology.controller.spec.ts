import { INestApplication } from '@nestjs/common'
import { Test, TestingModule } from '@nestjs/testing'
import request from 'supertest'
import { OwlOntologyModule } from './owl-ontology.module'
import { OwlOntologyService } from './services/owl-ontology.service'

describe('OwlOntologyController', () => {
  let app: INestApplication
  let owlOntologyService: OwlOntologyService

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [OwlOntologyModule]
    }).compile()

    owlOntologyService = moduleRef.get<OwlOntologyService>(OwlOntologyService)
    ;(owlOntologyService as any).ontologyBasePath = `${__dirname}/../tests/fixtures/owl`

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  it(
    'should return the latest development OWL ontology',
    () =>
      request(app.getHttpServer())
        .get('/owl')
        .expect(200)
        .expect('Content-Type', 'text/turtle; charset=utf-8')
        .then(response => {
          expect(response.text.indexOf('https://w3id.org/gaia-x/development#')).toBeGreaterThan(-1)
          expect(response.text.indexOf('gx:LegalPerson a owl:Class')).toBeGreaterThan(-1)
        }),
    10000
  )

  it(
    'should return the 1234 version OWL ontology',
    () =>
      request(app.getHttpServer())
        .get('/owl/1234')
        .expect(200)
        .expect('Content-Type', 'text/turtle; charset=utf-8')
        .then(response => {
          expect(response.text.indexOf('https://w3id.org/gaia-x/1234#')).toBeGreaterThan(-1)
          expect(response.text.indexOf('gx:LegalPerson a owl:Class')).toBeGreaterThan(-1)
        }),
    10000
  )

  it(
    'should throw a 404 error when querying a non-existent version',
    () => request(app.getHttpServer()).get('/owl/non-existent').expect(404).expect('Content-Type', 'application/json; charset=utf-8'),
    10000
  )
})
