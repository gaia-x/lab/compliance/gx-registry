import { Controller, Get, Param, Response } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { Response as Res } from 'express'
import { OwlOntologyApiResponse } from './decorators/owl-ontology-api-response.decorator'
import { OwlOntologyService } from './services/owl-ontology.service'

@ApiTags('OWL-Ontology')
@Controller({ path: '/owl' })
export class OwlOntologyController {
  constructor(private readonly owlOntologyService: OwlOntologyService) {}

  @OwlOntologyApiResponse('Get a version of the Gaia-X OWL ontology in Turtle format')
  @Get(':version?')
  getShape(@Response({ passthrough: true }) res: Res, @Param('version') version?: string): string {
    try {
      res.set({ 'Content-Type': 'text/turtle' })

      return this.owlOntologyService.getOwlFile(version)
    } catch (e) {
      throw e
    }
  }
}
