import { Module } from '@nestjs/common'
import { OwlOntologyController } from './owl-ontology.controller'
import { OwlOntologyService } from './services/owl-ontology.service'

@Module({
  imports: [],
  controllers: [OwlOntologyController],
  providers: [OwlOntologyService]
})
export class OwlOntologyModule {}
