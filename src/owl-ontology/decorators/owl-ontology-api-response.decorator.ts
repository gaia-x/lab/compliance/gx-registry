import { applyDecorators } from '@nestjs/common'
import { ApiBadRequestResponse, ApiOkResponse, ApiOperation, ApiParam } from '@nestjs/swagger'

export function OwlOntologyApiResponse(summary: string) {
  return applyDecorators(
    ApiOperation({ summary }),
    ApiParam({
      name: 'version',
      allowEmptyValue: true,
      required: false,
      description: 'the version of the OWL ontology to request (defaults to `development`)',
      examples: {
        development: {
          value: 'development',
          description: 'For the latest development version'
        },
        '2406': {
          value: '2406',
          description: 'For version 24.06 of the ontology'
        }
      }
    }),
    ApiBadRequestResponse({ description: 'OWL ontology not found' }),
    ApiOkResponse({ description: 'OWL ontology serialised as Turtle' })
  )
}
