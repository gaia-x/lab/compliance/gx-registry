import { Injectable, NotFoundException } from '@nestjs/common'
import fs from 'fs'

@Injectable()
export class OwlOntologyService {
  private ontologyBasePath = '/data/ipfs/registry'

  getOwlFile(version: string = 'development'): string {
    try {
      const filePath: string = `${this.ontologyBasePath}/${version}/owl/trustframework.ttl`
      fs.accessSync(filePath)

      return fs.readFileSync(filePath).toString()
    } catch {
      throw new NotFoundException(`Desired OWL ontology version '${version}' not found or not accessible`)
    }
  }
}
