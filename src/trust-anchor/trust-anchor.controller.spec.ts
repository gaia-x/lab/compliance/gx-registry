import { ConflictException, NotFoundException } from '@nestjs/common'
import { Test, TestingModule } from '@nestjs/testing'
import { Response as Res } from 'express'
import { CertificateChainDto, TrustAnchorRequestDto, TrustAnchorResponseDto, TrustStates } from './dto'
import { CertChainTransformPipe } from './pipes'
import { TrustAnchorService } from './services'
import { TrustAnchorController } from './trust-anchor.controller'

describe('TrustAnchorController', () => {
  let controller: TrustAnchorController
  let service: TrustAnchorService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TrustAnchorController],
      providers: [
        CertChainTransformPipe,
        {
          provide: TrustAnchorService,
          useValue: {
            getTrustAnchorsXml: jest.fn(),
            findTrustAnchor: jest.fn(),
            validateCertChain: jest.fn()
          }
        }
      ]
    }).compile()

    controller = module.get<TrustAnchorController>(TrustAnchorController)
    service = module.get<TrustAnchorService>(TrustAnchorService)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  it('should return XML data when trust anchors are found without version', async () => {
    const res = { setHeader: jest.fn(), status: jest.fn().mockReturnThis(), send: jest.fn() } as unknown as Res
    const xmlData = '<xml>data</xml>'
    jest.spyOn(service, 'getTrustAnchorsXml').mockReturnValue(xmlData)

    const result = await controller.getTrustAnchors(res)

    expect(service.getTrustAnchorsXml).toHaveBeenCalledWith(undefined)
    expect(res.setHeader).toHaveBeenCalledWith('Content-Type', 'application/xml')
    expect(result).toBe(xmlData)
  })

  it('should return XML data when trust anchors are found with a specific version', async () => {
    const res = { setHeader: jest.fn(), status: jest.fn().mockReturnThis(), send: jest.fn() } as unknown as Res
    const xmlData = '<xml>data</xml>'
    const version = '2404'
    jest.spyOn(service, 'getTrustAnchorsXml').mockReturnValue(xmlData)

    const result = await controller.getTrustAnchors(res, version)

    expect(service.getTrustAnchorsXml).toHaveBeenCalledWith(version)
    expect(res.setHeader).toHaveBeenCalledWith('Content-Type', 'application/xml')
    expect(result).toBe(xmlData)
  })

  it('should return 404 when trust anchors are not found', async () => {
    const res = { setHeader: jest.fn(), status: jest.fn().mockReturnThis(), send: jest.fn() } as unknown as Res
    jest.spyOn(service, 'getTrustAnchorsXml').mockImplementation(() => {
      throw new NotFoundException(`Trust anchors not found.`)
    })

    await controller.getTrustAnchors(res)

    expect(res.status).toHaveBeenCalledWith(404)
    expect(res.send).toHaveBeenCalledWith(`Trust anchors not found.`)
  })

  it('should return 404 when trust anchors are not found for a specific version', async () => {
    const res = { setHeader: jest.fn(), status: jest.fn().mockReturnThis(), send: jest.fn() } as unknown as Res
    const version = '2404'
    jest.spyOn(service, 'getTrustAnchorsXml').mockImplementation(() => {
      throw new NotFoundException(`Trust anchors not found.`)
    })

    await controller.getTrustAnchors(res, version)

    expect(res.status).toHaveBeenCalledWith(404)
    expect(res.send).toHaveBeenCalledWith(`Trust anchors not found.`)
  })

  it('should return trust anchor data for a valid request', async () => {
    const trustAnchorRequestDto: TrustAnchorRequestDto = { certificate: 'valid-cert' }
    const trustAnchorResponse: TrustAnchorResponseDto = {
      trustState: TrustStates.Trusted,
      trustedForAttributes: 'Attribute',
      trustedAt: Date.now()
    }

    jest.spyOn(service, 'findTrustAnchor').mockResolvedValue(trustAnchorResponse)

    const result = await controller.findTrustAnchor(trustAnchorRequestDto)

    expect(service.findTrustAnchor).toHaveBeenCalledWith(trustAnchorRequestDto)
    expect(result).toBe(trustAnchorResponse)
  })

  it('should throw NotFoundException for an invalid request', async () => {
    const trustAnchorRequestDto: TrustAnchorRequestDto = { certificate: 'invalid-cert' }
    jest.spyOn(service, 'findTrustAnchor').mockImplementation(() => {
      throw new NotFoundException('Trust Anchor not found.')
    })

    await expect(controller.findTrustAnchor(trustAnchorRequestDto)).rejects.toThrow(new NotFoundException('Trust Anchor not found.'))
  })

  it('should return true for a valid certificate chain', async () => {
    const certificateChainRaw: CertificateChainDto = { certs: ['cert1', 'cert2'] }
    jest.spyOn(service, 'validateCertChain').mockResolvedValue(true)

    const result = await controller.verifyTrustAnchorChainRaw(certificateChainRaw)

    expect(service.validateCertChain).toHaveBeenCalledWith(certificateChainRaw.certs)
    expect(result).toEqual({ result: true })
  })

  it('should throw ConflictException for an invalid certificate chain', async () => {
    const certificateChainRaw: CertificateChainDto = { certs: ['cert1', 'cert2'] }
    jest.spyOn(service, 'validateCertChain').mockImplementation(() => {
      throw new ConflictException('Unable to validate certificate chain', 'Error message')
    })

    await expect(controller.verifyTrustAnchorChainRaw(certificateChainRaw)).rejects.toThrow(
      new ConflictException('Unable to validate certificate chain', 'Error message')
    )
  })

  it('should return true for a valid certificate chain URI', async () => {
    const certificateChainRaw: CertificateChainDto = { certs: ['cert1', 'cert2'] }
    jest.spyOn(service, 'validateCertChain').mockResolvedValue(true)

    const result = await controller.verifyTrustAnchorChain(certificateChainRaw)

    expect(service.validateCertChain).toHaveBeenCalledWith(certificateChainRaw.certs)
    expect(result).toEqual({ result: true })
  })

  it('should throw ConflictException for an invalid certificate chain URI', async () => {
    const certificateChainRaw: CertificateChainDto = { certs: ['cert1', 'cert2'] }
    jest.spyOn(service, 'validateCertChain').mockImplementation(() => {
      throw new ConflictException('Unable to validate certificate chain', 'Error message')
    })

    await expect(controller.verifyTrustAnchorChain(certificateChainRaw)).rejects.toThrow(
      new ConflictException('Unable to validate certificate chain', 'Error message')
    )
  })
})
