import { Body, ConflictException, Controller, Get, HttpCode, HttpStatus, NotFoundException, Param, Post, Response } from '@nestjs/common'
import { Header } from '@nestjs/common/decorators/http'
import { ApiConflictResponse, ApiConsumes, ApiNotFoundResponse, ApiTags } from '@nestjs/swagger'
import { Response as Res } from 'express'
import { certificateChainRequest, certificateChainUriRequest, trustAnchorV2Request } from '../tests/fixtures/certificates.json'
import { TrustAnchorApiResponse, TrustAnchorApiVersionResponse } from './decorators'
import { CertsBody } from './decorators/certs-request.decorator'
import { CertificateChainDto, TrustAnchorChainUriRequestDto, TrustAnchorRequestDto } from './dto'
import { CertChainUriTransformPipe } from './pipes'
import { TrustAnchorService } from './services'

@ApiTags('TrustAnchor')
@Controller({ path: '/api/trustAnchor' })
export class TrustAnchorController {
  constructor(private readonly trustAnchorService: TrustAnchorService) {}

  @Get('/:version?')
  @Header('Content-Type', 'application/xml')
  @TrustAnchorApiVersionResponse('Get the list (format ETSI TS 119 612) of all Trust Anchors used by the registry')
  async getTrustAnchors(@Response({ passthrough: true }) res: Res, @Param('version') version?: string) {
    try {
      const xmlData = this.trustAnchorService.getTrustAnchorsXml(version)
      res.setHeader('Content-Type', 'application/xml')
      return xmlData
    } catch (error) {
      if (error instanceof NotFoundException) {
        res.status(404).send(error.message)
      } else {
        res.status(500).send('An unexpected error occurred.')
      }
    }
  }

  @Post()
  @TrustAnchorApiResponse('Search for a TrustAnchor certificate in the registry', TrustAnchorRequestDto, {
    trustAnchor: { summary: 'Example Certificate', value: trustAnchorV2Request }
  })
  @HttpCode(HttpStatus.OK)
  @ApiNotFoundResponse({ description: `TrustAnchor was not be found in the registry` })
  async findTrustAnchor(@Body() trustAnchorRequestDto: TrustAnchorRequestDto) {
    return this.trustAnchorService.findTrustAnchor(trustAnchorRequestDto)
  }

  @Post('chain')
  @TrustAnchorApiResponse('Verify root of a certificate chain to be a TrustAnchor in the registry', String, {
    certChain: { summary: 'Example Certificate Chain in JSON', value: certificateChainRequest },
    certChainRaw: { summary: 'Example raw Certificate Chain', value: certificateChainRequest.certs }
  })
  @ApiConsumes('application/json', 'text/plain', 'application/x-pem-file', 'application/x-x509-ca-cert', 'x-x509-user-cert')
  @ApiConflictResponse({ description: `Root for the certificate chain could not be verified as a TrustAnchor in the registry` })
  async verifyTrustAnchorChainRaw(@CertsBody() certificateChainRaw: CertificateChainDto): Promise<{ result: true }> {
    return this.validateCertificateChain(certificateChainRaw.certs)
  }

  @Post('chain/file')
  @TrustAnchorApiResponse(
    'Verify root of a certificate chain, provided as a file at uri, to be a TrustAnchor in the registry',
    TrustAnchorChainUriRequestDto,
    {
      certChain: { summary: 'Example Certificate Chain', value: certificateChainUriRequest }
    }
  )
  @ApiConflictResponse({ description: `Root for the certificate chain could not be verified as a TrustAnchor in the registry` })
  async verifyTrustAnchorChain(@Body(CertChainUriTransformPipe) certificateChainRaw: CertificateChainDto): Promise<{ result: true }> {
    return this.validateCertificateChain(certificateChainRaw.certs)
  }

  async validateCertificateChain(certificates: string[]): Promise<{ result: true }> {
    let result: boolean
    try {
      result = await this.trustAnchorService.validateCertChain(certificates)
    } catch (error) {
      throw new ConflictException('Unable to validate certificate chain', error.message + '- ' + error?.response?.error)
    }

    if (!result) throw new ConflictException(result)

    return { result }
  }
}
