import { BadRequestException, ConflictException, Injectable, Logger, NotFoundException, OnModuleInit } from '@nestjs/common'
import { Cron, CronExpression } from '@nestjs/schedule'
import * as crypto from 'crypto'
import { XMLParser } from 'fast-xml-parser'
import fs from 'fs'
import { ValidationResult } from 'joi'
import { KEYUTIL, X509, hextob64 } from 'jsrsasign'
import * as path from 'path'
import { TrustedList } from '../../common/interfaces'
import { isEmpty } from '../../common/util'
import { TrustAnchorRequestDto, TrustAnchorResponseDto, TrustStates } from '../dto'
import { IssuersRevocationListService } from './issuers-revocation-list.service'

@Injectable()
export class TrustAnchorService implements OnModuleInit {
  private readonly logger = new Logger(TrustAnchorService.name)
  private trustAnchorCache = new Set<string>() // Cache to store hashed certificates
  private readonly OID_EV_SSL = '2.23.140.1.1'
  private aisblCert: X509
  private ontologyVersion: string
  private ipfsBasePath: string
  private listIssueDateTime: Date | null = null

  constructor(private readonly issuersRevocationListService: IssuersRevocationListService) {
    this.ontologyVersion = process.env.ONTOLOGY_VERSION || 'development'
    this.ipfsBasePath = `/data/ipfs/registry`
  }

  async onModuleInit(): Promise<void> {
    this.handleCron()
  }

  @Cron(CronExpression.EVERY_3_HOURS)
  async handleCron() {
    this.logger.log('Reloading trust anchor cache...')
    await this.loadTrustAnchorsCache()
  }

  /**
   * Validates a chain of X509 certificates
   * @param base64Certs the raw base64 encoded certificate strings
   * @returns {boolean} the verification result
   */
  public async validateCertChain(base64Certs: string[]): Promise<boolean> {
    const jsrsasignCerts = base64Certs.map(cert => this.base64ToX509(cert))
    this.verifyLeafOIDEVSSL(jsrsasignCerts[0])
    await this.verifyCertificateChain(jsrsasignCerts)
    await this.verifyRootCertificate(jsrsasignCerts)

    if (!this.issuersRevocationListService.areCertificatesNotRevoked(jsrsasignCerts)) {
      throw new ConflictException('Issuer is in revocation list')
    }

    return true // the chain is valid
  }

  private async verifyCertificateChain(jsrsasignCerts: X509[]) {
    for (let i = 0; i < jsrsasignCerts.length - 1; i++) {
      const childCert = jsrsasignCerts[i] // Child certificate
      const issuerCert = jsrsasignCerts[i + 1] // Issuer's certificate

      if (!this.verifySignature(childCert, issuerCert)) {
        throw new ConflictException(`Certificate ${childCert.getSerialNumberHex()} issued by ${childCert.getIssuer().str} invalid signature.`)
      }
    }

    return true // the chain is valid
  }

  /**
   * Verifies that a child certificate is signed by its issuer.
   * @param {X509} childCert Child certificate in X509 format.
   * @param {X509} issuerCert Issuer certificate in X509 format.
   * @returns {boolean} Returns true if the signature is valid, false otherwise.
   */
  private verifySignature(childCert: X509, issuerCert: X509): boolean {
    try {
      // Extract public key from issuer certificate
      const issuerPublicKey = KEYUTIL.getKey(issuerCert.getPublicKey())
      // verify that child certificate is signed by issuer
      return childCert.verifySignature(issuerPublicKey)
    } catch (error) {
      this.logger.error('Signature verification failed', error)
      return false
    }
  }

  private async verifyRootCertificate(jsrsasignCerts: X509[]) {
    const rootCert = jsrsasignCerts[jsrsasignCerts.length - 1] // Root certificate

    try {
      // Verify the root certificate is self-signed
      const rootPublicKey = KEYUTIL.getKey(rootCert.getPublicKey())
      const isSelfSigned = rootCert.verifySignature(rootPublicKey)

      if (!isSelfSigned) {
        throw new ConflictException('Root certificate is not self-signed.')
      }

      // Verify the root certificate is in the trusted CAStore
      if (!this.isCertificateTrusted(rootCert)) {
        throw new NotFoundException('Root certificate not trusted.')
      }
    } catch (error) {
      throw new ConflictException('Unable to validate cert chain root certificate: ' + error.message)
    }
    return true
  }

  private isCertificateTrusted(certToCheck: X509): boolean {
    const b64Cert = hextob64(certToCheck.hex)
    const hashedCert = this.hashCertificate(b64Cert)
    return this.trustAnchorCache.has(hashedCert)
  }

  /**
   * Searches for a trustAnchor in the cached hashed list given a certain certificate and returns a trustAnchorResponse if one was found.
   * Will throw a 404 exception in case no corresponding trustAnchor could be found.
   * @param trustAnchorData the body containing a certificate
   * @returns {Promise<TrustAnchorResponseDto>} the prepared trustAnchorResponse
   */
  public async findTrustAnchor(trustAnchorData: ValidationResult<TrustAnchorRequestDto>['value']): Promise<TrustAnchorResponseDto> {
    if (isEmpty(trustAnchorData)) throw new BadRequestException('Request body invalid.')
    const { certificate } = trustAnchorData
    const x509 = this.base64ToX509(certificate)

    const findTrustAnchor = this.isCertificateTrusted(x509)

    if (!findTrustAnchor) throw new NotFoundException('Trust Anchor not found.')

    return await this.prepareTrustAnchorResponse()
  }

  /**
   * Prepares a trustAnchorResponse given a certain trustAnchor
   * @returns {Promise<TrustAnchorResponseDto>} the prepared trustAnchorResponse
   */
  private async prepareTrustAnchorResponse(): Promise<TrustAnchorResponseDto> {
    return {
      trustState: TrustStates.Trusted, // we only use Trusted for compatibility but states could be extended. All anchors in our ETSI list are Trusted.
      trustedForAttributes: '/.*/gm',
      trustedAt: this.listIssueDateTime ? this.listIssueDateTime.getTime() : null
    }
  }

  public async loadTrustAnchorsCache(): Promise<void> {
    try {
      const notaryCertPath = path.join(this.ipfsBasePath, this.ontologyVersion, 'x509CertificateChain.pem')
      const notaryCert = fs.readFileSync(notaryCertPath, 'utf8')

      this.aisblCert = new X509()
      this.aisblCert.readCertPEM(notaryCert)

      const filePath = path.join(this.ipfsBasePath, this.ontologyVersion, 'trust-anchors/trust-anchors.xml')
      const xmlData = fs.readFileSync(filePath, 'utf-8')

      const parser = new XMLParser({
        ignoreAttributes: false,
        removeNSPrefix: true,
        isArray: name => name === 'TrustServiceProvider'
      })

      const jsonObj: TrustedList = parser.parse(xmlData)
      const { ListIssueDateTime } = jsonObj.TrustServiceStatusList.SchemeInformation

      const anchors = jsonObj.TrustServiceStatusList.TrustServiceProviderList.TrustServiceProvider.map(
        provider => provider.TSPServices?.TSPService?.ServiceInformation?.ServiceDigitalIdentity?.DigitalId?.X509Certificate
      ).filter((cert): cert is string => !!cert) // Filter out undefined certificates

      if (anchors.length === 0) {
        this.logger.error('No valid TrustServiceProviders found.')
        return
      }

      this.trustAnchorCache.clear()
      anchors.forEach(anchor => this.trustAnchorCache.add(this.hashCertificate(anchor)))

      this.listIssueDateTime = new Date(ListIssueDateTime)

      this.logger.log(`Trust anchor cache has been populated with ${anchors.length} certificates`)
    } catch (error) {
      this.logger.error('Failed to load trust anchor cache', error)
    }
  }

  /**
   * Get trust anchors as XML
   * @param {string} [version] - The version to use instead of the default ontology version
   * @returns {string} the XML data
   */
  public getTrustAnchorsXml(version?: string): string {
    const ontologyVersion = version || this.ontologyVersion
    const filePath = path.join(this.ipfsBasePath, ontologyVersion, 'trust-anchors', 'trust-anchors.xml')

    try {
      fs.accessSync(filePath)
      this.logger.log(`Found trust anchors at ${filePath}. Reading the file.`)
      return fs.readFileSync(filePath, 'utf-8')
    } catch (error) {
      this.logger.log(`Error while accessing or reading ${filePath}`, error)
      throw new NotFoundException(`Trust anchors not found for version ${ontologyVersion}.`)
    }
  }

  private base64ToX509(b64Certificate: string): X509 {
    const pem = this.addPEMHeadersAndFooters(b64Certificate)
    const x509 = new X509()
    x509.readCertPEM(pem)
    return x509
  }

  private hashCertificate(cert: string): string {
    const hash = crypto.createHash('sha256')
    hash.update(cert)
    return hash.digest('hex')
  }

  private verifyLeafOIDEVSSL(certificate: X509) {
    if ('false' === process.env.evsslonly || certificate.hex === this.aisblCert?.hex) {
      return
    }
    let certificatePolicies
    try {
      certificatePolicies = certificate.getExtCertificatePolicies()
    } catch (error) {
      throw new ConflictException('Invalid certificate for signature', 'The leaf certificate provided is not EV-SSL - 2.23.140.1.1 OID is missing')
    }
    if (!certificatePolicies || !certificatePolicies.array || !certificatePolicies.array.find(policy => policy.policyoid === this.OID_EV_SSL)) {
      throw new ConflictException('Invalid certificate for signature', 'The leaf certificate provided is not EV-SSL - 2.23.140.1.1 OID is missing')
    }
  }

  private addPEMHeadersAndFooters(cert: string) {
    return `-----BEGIN CERTIFICATE-----
${cert}
-----END CERTIFICATE-----`
  }
}
