import { BadRequestException, ExecutionContext, createParamDecorator } from '@nestjs/common'
import { Request } from 'express'
import getRawBody from 'raw-body'
import { CertificateChainDto } from '../dto'
import { CertChainTransformPipe } from '../pipes'

/** Read either a certificate chain as text or wrapped inside JSON {"certs": string | string[]} */
export const CertsBody = createParamDecorator((_: unknown, ctx: ExecutionContext) => readRequest(ctx.switchToHttp().getRequest()))

async function readRequest(request: Request, opts?: { reader: (request: Request) => Promise<Buffer> }): Promise<CertificateChainDto> {
  try {
    if (request.readable) {
      request.body = (await (opts?.reader ?? getRawBody)(request)).toString('utf8').trim()
      if (request.body.startsWith('{') && request.body.endsWith('}')) {
        request.body = JSON.parse(request.body)
      }
    }
    return new CertChainTransformPipe().transform(request.body)
  } catch (error) {
    throw new BadRequestException('Certificate chain could not be transformed.')
  }
}
