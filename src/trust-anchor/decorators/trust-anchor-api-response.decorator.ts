import { applyDecorators, HttpCode, HttpStatus } from '@nestjs/common'
import { ApiBadRequestResponse, ApiBody, ApiOkResponse, ApiOperation, ApiParam, ApiProperty } from '@nestjs/swagger'
import { ExamplesObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface'

class TrustAnchorApiResponseType {
  @ApiProperty()
  result: boolean
}
export function TrustAnchorApiResponse(summary: string, dto: any, examples?: ExamplesObject) {
  return applyDecorators(
    HttpCode(HttpStatus.OK),
    ApiOperation({ summary }),
    ApiBody({ type: dto, examples, description: summary }),
    ApiBadRequestResponse({ description: 'Invalid request payload' }),
    ApiOkResponse({
      description: `TrustAnchor or root for chain was found in the registry`,
      type: TrustAnchorApiResponseType
    })
  )
}

export function TrustAnchorApiVersionResponse(summary: string) {
  return applyDecorators(
    ApiOperation({ summary }),
    ApiParam({
      name: 'version',
      allowEmptyValue: true,
      required: false,
      description: 'Version of the trust anchor list (defaults to the default ontology version)',
      examples: {
        empty: {
          value: '',
          description: 'Defaults to version set in the deployment environment'
        },
        development: {
          value: 'development',
          description: 'For the trust anchors of the development ontology'
        },
        '2406': {
          value: '2406',
          description: 'For the trust anchors of the 24.06 ontology'
        }
      }
    }),
    ApiBadRequestResponse({ description: 'Trust anchors not found' }),
    ApiOkResponse({ description: 'Trust anchors as XML' })
  )
}
