export class TrustAnchorResponseDto {
  public trustState: TrustStates
  public trustedForAttributes?: string
  public trustedAt?: number
}

// ETSI described trust state added for future use
export enum TrustStates {
  Trusted = 'trusted', // Gaia-X trust state
  Untrusted = 'untrusted', // Gaia-X trust state
  UnderSupervision = 'undersupervision', // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/undersupervision
  SupervisionInCessation = 'supervisionincessation', // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/supervisionincessation
  SupervisionCeased = 'supervisionceased', // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/supervisionceased
  SupervisionRevoked = 'supervisionrevoked', // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/supervisionrevoked
  Accredited = 'accredited', // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/accredited
  AccreditationCeased = 'accreditationceased', // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/accreditationceased
  AccreditationRevoked = 'accreditationrevoked', // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/accreditationrevoked
  Granted = 'granted', // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/granted
  Withdrawn = 'withdrawn', // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/withdrawn
  SetByNationalLaw = 'setbynationallaw', // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/setbynationallaw
  RecognizedAtNationalLevel = 'recognisedatnationallevel', // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/recognisedatnationallevel
  DeprecatedByNationalLaw = 'deprecatedbynationallaw', // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/deprecatedbynationallaw
  DeprecatedAtNationalLevel = 'deprecatedatnationallevel' // http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/deprecatedatnationallevel
}
