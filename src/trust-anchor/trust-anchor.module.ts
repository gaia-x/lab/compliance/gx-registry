import { HttpModule } from '@nestjs/axios'
import { CacheModule } from '@nestjs/cache-manager'
import { Module } from '@nestjs/common'
import { ScheduleModule } from '@nestjs/schedule'
import { IpfsModule } from '../ipfs/ipfs.module'
import { CertChainTransformPipe } from './pipes'
import { TrustAnchorService } from './services'
import { IssuersRevocationListService } from './services/issuers-revocation-list.service'
import { TrustAnchorController } from './trust-anchor.controller'

@Module({
  imports: [
    HttpModule,
    ScheduleModule.forRoot(),
    CacheModule.register(),
    IpfsModule //required to have the cert at startup
  ],
  controllers: [TrustAnchorController],
  providers: [IssuersRevocationListService, TrustAnchorService, CertChainTransformPipe],
  exports: [TrustAnchorService]
})
export class TrustAnchorModule {}
