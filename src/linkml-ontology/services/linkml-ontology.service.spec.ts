import { NotFoundException } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import fs from 'fs'
import { LinkMLOntologyService } from './linkml-ontology.service'

jest.mock('fs')

describe('LinkMLOntologyService', () => {
  let service: LinkMLOntologyService

  beforeEach(async () => {
    jest.resetAllMocks()

    const moduleRef = await Test.createTestingModule({
      providers: [LinkMLOntologyService]
    }).compile()

    service = moduleRef.get<LinkMLOntologyService>(LinkMLOntologyService)
  })

  it('should collect a development LinkML ontology by default', () => {
    jest.mocked(fs.accessSync).mockReturnValue(undefined)
    jest.mocked(fs.readFileSync).mockReturnValue('prefixes:\ngx:\nprefix_prefix: gx\nprefix_reference: https://w3id.org/gaia-x/development#')

    expect(service.getLinkMLOntology()).toEqual('prefixes:\ngx:\nprefix_prefix: gx\nprefix_reference: https://w3id.org/gaia-x/development#')

    expect(fs.accessSync).toHaveBeenLastCalledWith('/data/ipfs/registry/development/linkml/linkml.yaml')
    expect(fs.readFileSync).toHaveBeenLastCalledWith('/data/ipfs/registry/development/linkml/linkml.yaml')
  })

  it('should collect a specific version of the LinkML ontology', () => {
    jest.mocked(fs.accessSync).mockReturnValue(undefined)
    jest.mocked(fs.readFileSync).mockReturnValue('prefixes:\ngx:\nprefix_prefix: gx\nprefix_reference: https://w3id.org/gaia-x/2404#')

    expect(service.getLinkMLOntology('2404')).toEqual('prefixes:\ngx:\nprefix_prefix: gx\nprefix_reference: https://w3id.org/gaia-x/2404#')

    expect(fs.accessSync).toHaveBeenLastCalledWith('/data/ipfs/registry/2404/linkml/linkml.yaml')
    expect(fs.readFileSync).toHaveBeenLastCalledWith('/data/ipfs/registry/2404/linkml/linkml.yaml')
  })

  it('should throw an error when the LinkML ontology does not exist', () => {
    jest.mocked(fs.accessSync).mockImplementation(() => {
      throw new Error('File is missing !')
    })

    expect(() => service.getLinkMLOntology('1234')).toThrow(
      new NotFoundException(`Desired LinkML ontology version '1234' not found or not accessible`)
    )

    expect(fs.accessSync).toHaveBeenLastCalledWith('/data/ipfs/registry/1234/linkml/linkml.yaml')
    expect(fs.readFileSync).not.toHaveBeenCalled()
  })
})
