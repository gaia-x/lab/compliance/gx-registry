import { Module } from '@nestjs/common'
import { LinkMLOntologyController } from './linkml-ontology.controller'
import { LinkMLOntologyService } from './services/linkml-ontology.service'

@Module({
  imports: [],
  controllers: [LinkMLOntologyController],
  providers: [LinkMLOntologyService]
})
export class LinkMLOntologyModule {}
