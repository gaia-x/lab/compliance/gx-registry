FROM node:20-alpine AS development-build-stage

RUN apk upgrade --no-cache

USER node
WORKDIR /usr/src/app

COPY --chown=node package*.json ./
RUN npm install

COPY --chown=node src src
COPY --chown=node nest-cli.json nest-cli.json
COPY --chown=node tsconfig.json tsconfig.json
COPY --chown=node tsconfig.build.json tsconfig.build.json

RUN ls -alih src/utils

RUN npm run build

RUN ls -alih dist/src/utils


# Production Stage
FROM node:20-alpine AS production-build-stage

ENV NODE_ENV=production

RUN apk upgrade --no-cache

USER node
WORKDIR /usr/src/app

COPY --from=development-build-stage --chown=node /usr/src/app/node_modules ./node_modules
COPY --from=development-build-stage --chown=node /usr/src/app/dist ./dist

RUN ls -alih dist/src/utils

COPY --chown=node package*.json ./

CMD ["node", "dist/src/main"]
